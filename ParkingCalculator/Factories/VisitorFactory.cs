﻿using ParkingCalculator.Models;
using System;

namespace ParkingCalculator.Factories
{
    public static class VisitorFactory
    {
        public static Visitor CreateVisitor(string firstName, string lastName, DateTime startDate, DateTime endDate, IParkingCalculator parkingCalculator)
        {
            return new Visitor
            {
                FirstName = firstName,
                LastName = lastName,
                Start = startDate,
                End = endDate,
                ParkingTicket = parkingCalculator
            };
        }
    }
}
