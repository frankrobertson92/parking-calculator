﻿using ParkingCalculator.Enums;
using ParkingCalculator.Interfaces;
using System;

namespace ParkingCalculator.Models
{
    public class Visitor : IUser, IParking
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public StayType StayType { get; set; }
        public IParkingCalculator ParkingTicket { get; set; }
    }
}
