﻿using System;

namespace ParkingCalculator.Services
{
    public static class Validate
    {
        public static bool ValidateDates(DateTime startDate, DateTime endDate)
        {
            if (startDate == null || endDate == null)
                return false;

            if (endDate < startDate)
                return false;

            if (endDate == startDate)
                return false;

            return true;
        }
    }
}
