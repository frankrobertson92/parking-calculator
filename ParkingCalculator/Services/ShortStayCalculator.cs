﻿using ParkingCalculator.Helpers;
using System;

namespace ParkingCalculator.Services
{
    public class ShortStayCalculator : IParkingCalculator
    {
        // Could be factored out into configuration / app setting
        private readonly double rate = 1.10;

        public double CalculateParkingCharge(DateTime startDate, DateTime endDate)
        {
            var hours = CalculatorHelper.BusinessTime(startDate, endDate).TotalHours;
            return Math.Round(hours * rate, 2);
        }
    }
}
