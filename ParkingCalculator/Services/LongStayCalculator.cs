﻿using System;

namespace ParkingCalculator.Services
{
    public class LongStayCalculator : IParkingCalculator
    {
        // Could be factored out into configuration / app setting
        private readonly double rate = 7.50;

        public double CalculateParkingCharge(DateTime startDate, DateTime endDate)
        {
            var totalDays = (endDate.Date - startDate.Date).Days + 1;
            var totalCost = totalDays * rate;

            return Math.Round(totalCost, 2);
        }
    }
}