﻿using System;

namespace ParkingCalculator
{
    public interface IParkingCalculator
    {
        double CalculateParkingCharge(DateTime startDate, DateTime endDate);
    }
}
