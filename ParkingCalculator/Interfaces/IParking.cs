﻿using ParkingCalculator.Enums;
using System;

namespace ParkingCalculator.Interfaces
{
    public interface IParking
    {
        DateTime Start { get; set; }
        DateTime End { get; set; }
        StayType StayType { get; set; }
        IParkingCalculator ParkingTicket { get; set; }
    }
}
