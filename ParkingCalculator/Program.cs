﻿using ParkingCalculator.Factories;
using ParkingCalculator.Models;
using ParkingCalculator.Services;
using System;
using System.Collections.Generic;

namespace ParkingCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Visitor> visitors = new List<Visitor>();
            visitors.Add(VisitorFactory.CreateVisitor("Frank", "Robertson", new DateTime(2017, 09, 07, 16, 50, 00), new DateTime(2017, 09, 09, 19, 15, 00), new ShortStayCalculator()));
            visitors.Add(VisitorFactory.CreateVisitor("Michael", "Scott", new DateTime(2017, 09, 07, 07, 50, 00), new DateTime(2017, 09, 09, 05, 20, 00), new LongStayCalculator()));
            foreach (Visitor visitor in visitors)
            {
                if (Validate.ValidateDates(visitor.Start, visitor.End))
                {
                    Console.WriteLine("£" + visitor.ParkingTicket.CalculateParkingCharge(visitor.Start, visitor.End));
                }
                else
                {
                    // Add some logging here
                    Console.WriteLine("Dates were not valid for visitor " + visitor.FirstName + " " + visitor.LastName + "\n");
                }
            }
        }
    }
}