using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParkingCalculator.Services;
using System;

namespace ParkingCalculator.Tests
{
    [TestClass]
    public class ParkingTests
    {
        [TestMethod]
        public void TestNonChargeableHours()
        {
            var shortStay = new ShortStayCalculator();
            Assert.AreEqual(shortStay.CalculateParkingCharge(new DateTime(2021, 08, 08, 0, 0, 0), new DateTime(2021, 08, 09, 0, 0, 0)), 0);
            Assert.AreEqual(shortStay.CalculateParkingCharge(new DateTime(2021, 12, 02, 0, 0, 0), new DateTime(2021, 12, 02, 0, 0, 10)), 0);
            
        }

        [TestMethod]
        public void TestLongStay()
        {
            var longStay = new LongStayCalculator();
            Assert.AreEqual(longStay.CalculateParkingCharge(new DateTime(2021, 06, 14, 0, 0, 0), new DateTime(2021, 06, 14, 0, 0, 1)), 7.50);
            Assert.AreEqual(longStay.CalculateParkingCharge(new DateTime(2021, 06, 14, 0, 0, 0), new DateTime(2021, 06, 15, 0, 0, 1)), 15);
        }
    }
}
