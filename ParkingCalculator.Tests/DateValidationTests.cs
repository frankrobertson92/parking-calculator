using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParkingCalculator.Services;
using System;

namespace ParkingCalculator.Tests
{
    [TestClass]
    public class DateValidationTests
    {
        [TestMethod]
        public void TestInvalidDates()
        {
            Assert.AreEqual(Validate.ValidateDates(new DateTime(2001, 1, 1, 1, 1, 1), new DateTime(2001, 1, 1, 1, 1, 1)), false);
            Assert.AreEqual(Validate.ValidateDates(new DateTime(2001, 1, 1, 1, 1, 1).AddSeconds(1), new DateTime(2001, 1, 1, 1, 1, 1)), false);
            Assert.AreEqual(Validate.ValidateDates(new DateTime(2001, 1, 1, 1, 1, 1).AddMinutes(1), new DateTime(2001, 1, 1, 1, 1, 1)), false);
        }
    }
}
